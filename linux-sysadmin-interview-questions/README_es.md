Linux System Administrator/DevOps Interview Questions
Preguntas para entrevistas de Administradores de sistemas linux
====================================================

A collection of linux sysadmin/devops interview questions. Feel free to contribute via pull requests, issues or email messages.

Una colecion de preguntas para tus candidatos.

## <a name='toc'>Table of Contents</a>

  1. [Contributors](#contributors)
  1. [General Questions](#general)
  1. [Simple Linux Questions](#simple)
  1. [Medium Linux Questions](#medium)
  1. [Hard Linux Questions](#hard)
  1. [Expert Linux Questions](#expert)
  1. [Networking Questions](#network)
  1. [MySQL Questions](#mysql)
  1. [DevOps Questions](#devop)
  1. [Fun Questions](#fun)
  1. [Demo Time](#demo)
  1. [Other Great References](#references)


####[[⬆]](#toc) <a name='contributors'>Contributors:</a>

* [moregeek](https://github.com/moregeek)
* [typhonius](https://github.com/typhonius)
* [schumar](https://github.com/schumar)
* [negesti](https://github.com/negesti)
* peter
* [andreashappe](https://github.com/andreashappe)
* [quatrix](https://github.com/quatrix)
* [biyanisuraj](https://github.com/biyanisuraj)
* [pedroguima](https://github.com/pedroguima)
* Ben


####[[⬆]](#toc) <a name='general'>Preguntas genericas:</a>

* Que aprendistes ayer? What did you learn yesterday/this week?
* Talk about your preferred development/administration environment. (OS, Editor, Browsers, Tools etc.)
* Tell me about the last major Linux project you finished.
* Tell me about the biggest mistake you've made in [some recent time period] and how you would do it differently today. What did you learn from this experience?
* Por que deveriamos escojerte? Why we must choose you?
* What function does DNS play on a network?
* Que es HTTP?
* Que es un HTTP Proxy y como funciona? What is an HTTP proxy and how does it work?
* Describe briefly how HTTPS works.
* What is SMTP? Give the basic scenario of how a mail message is delivered via SMTP.
* What is RAID? What is RAID0, RAID1, RAID5, RAID10?
* What is a level 0 backup? What is an incremental backup?
* Describe the general file system hierarchy of a Linux system.


####[[⬆]](#toc) <a name='simple'>Preguntas que son simple:</a>

* Cual es el nombre y  UID de of the administrator user?
root & 0
* How to list all files, including hidden ones, in a directory?
* What is the Unix/Linux command to remove a directory and its contents?
rm -rf "dir"
* Which command will show you free/used memory? Does free memory exist on Linux?
free -m
* How to search for the string "I drink coffee black" in files of a directory recursively?
grep -r
* How to connect to a remote server or what is SSH?
ssh "hostname or ip"
* How to get all environment variables and how can you use them?
* I get "command not found" when I run ```ifconfig -a```. What can be wrong?
* What happens if I type TAB-TAB?
* What command will show the available disk space on the Unix/Linux system?
df
* What commands do you know that can be used to check DNS records?
dig
* What Unix/Linux command will alter a files ownership
* What Unix/Linux command will alter a files permissions?

* What does ```chmod +x FILENAME```do?
* What does the permission 0750 on a file mean?
* What does the permission 0750 on a directory mean?
* How to add a new system user without login permissions?
* How to add/remove a group from a user?
* What is a bash alias?
* How do you set the mail address of the root/a user?
* Que CRTL-c hace? What does CTRL-c do?
* What is in /etc/services?
* How to redirect STDOUT and STDERR in bash? (> /dev/null 2>&1)
* What is the difference between UNIX and Linux.
* What is the difference between Telnet and SSH?
* Explain the three load averages and what do they indicate. What command can be used to view the load averages?
* Can you name a lower-case letter that is not a valid option for GNU ```ls```?
* What is an LVM  ?
LVM stands for Logical Volume Manager , to resize filesystem’s size online we required LVM partition in Linux. Size of LVM partition can be extended and reduced using the lvextend & lvreduce commands respectively.

How do you check CPU stats ?
the ```vmstat``` command  can display the physical and virtual memory statistics respectively.
With the help of ```sar``` command we see the CPU utilization & other stats.

What does Sar provides and at which location Sar logs are stored ?

 Sar Collect, report, or save system activity information. The default version of the sar command (CPU utilization report) might be one of the first facilities the  user  runs  to  begin system  activity investigation, because it monitors major system resources. If CPU utilization is near 100 percent (user + nice + system), the workload sampled is CPU-bound.

By  default log files of Sar command  is located at  /var/log/sa/sadd file, where the dd parameter indicates the current day.

* How to increase the size of LVM partition ?

 Below are the Logical Steps :
– Use the lvextend command (lvextend -L +100M /dev/<Name of the LVM Partition> , in this example we are extending the size by 100MB.
– resize2fs /dev/<Name of the LVM Partition>
– check the size of partition using ‘df -h’ command

 * How to reduce or shrink the size of LVM partition ?


-Umount the filesystem using umount command,
-use resize2fs command , e.g resiz2fs /dev/mapper/myvg-mylv 10G
-Now use the lvreduce command , e.g lvreduce -L 10G /dev/mapper/myvg-mylv

Above Command will shrink the size & will make the filesystem size 10GB.

* How to create partition from the raw disk ?(we have to add gparted)

 Using fdisk utility we can create partitions from the raw disk.Below are the steps to create partition from the raw dsik :
– fdisk  /dev/hd* (IDE) or /dev/sd* (SCSI)
– Type n to create a new partition
–  After creating partition , type w command to write the changes to the partition table.

* Where the kernel modules are located ?

 The ‘/lib/modules/kernel-version/’ directory stores all kernel modules or compiled drivers in Linux operating system. Also with ‘lsmod’ command we can see all the installed kernel modules.

* Que es un umask? What is umask ?

 umask stands for ‘User file creation mask’, which determines the settings of a mask that controls which file permissions are set for files and directories when they are created.

 How to set the umask permanently for a user?

 To set this value permanently for a user, it has to be put in the appropriate profile file which depends on the default shell of the user.

* How to change the default run level in linux ? (This has to be changed since is different with systemd)

 To change the run level we have to edit the file “/etc/inittab” and change initdefault entry ( id:5:initdefault:). Using ‘init’ command we change the run level temporary like ‘init 3’ , this command will move the system in runlevl 3.

* How to share a directory using nfs ?

 To share a directory using nfs , first edit the configuration file ‘/etc/exportfs’ , add a entry like
‘/<directory-name>  <ip or Network>(Options)’ and then restart the nfs service.

* How to check and mount nfs share ?

 Using ‘showmount’ command we can see what directories are shared via nfs e.g ‘showmount -e <ip address of nfs server>’.Using mount command we can mount the nfs share on linux machine.

* What are the default ports used for DNS?
53
* What are the default ports used for DHCP?
67 & 68
* What are the default ports used for SSH?
22  
* What is Network Bonding ?

 Network bonding is the aggregation of multiple Lan cards into a single bonded interface to provide fault tolerance and high performance. Network bonding is also known as NIC Teaming.

* What  are the different modes of Network bonding in Linux ?

 Below are list of modes used in Network Bonding :

balance-rr or 0 – round-robin mode for fault tolerance and load balancing.
active-backup or 1 – Sets active-backup mode for fault tolerance.
balance-xor or 2 – Sets an XOR (exclusive-or) mode for fault tolerance and load balancing.
broadcast or 3 – Sets a broadcast mode for fault tolerance. All transmissions are sent on all slave interfaces.
802.3ad or 4  – Sets an IEEE 802.3ad dynamic link aggregation mode. Creates aggregation groups that share the same speed & duplex settings.
balance-tlb or 5 –  Sets a Transmit Load Balancing (TLB) mode for fault tolerance & load balancing.
balance-alb or 6 –  Sets an Active Load Balancing (ALB) mode for fault tolerance & load balancing.

*  How to check and verify the status the bond interface.

 Using the command ‘cat /proc/net/bonding/bond0’ , we can check which mode is enabled and what lan cards are used in this bond. In this example we have one only one bond interface but we can have multiple bond interface like bond1,bond2 and so on.

* How to check default route and routing table ?

 Using the Commands ‘netstat -nr’ and ‘route -n’ we can see the default route and routing tables.

*  How to check which ports are listening in my Linux Server ?

  Use the Command ‘netstat –listen’ and ‘lsof -i’

* List the services that are enabled at a particular run level in linux server ?

 With the help of command ‘chkconfig –list | grep 5:on’ we can list all the service that are enabled in run level5. For other run levels just replace 5 with the respective run level.

* How to enable a service at a particular run level ?

 We can enable a service using the Command ‘chkconfig <Service-Name> on –level 3’

* Como se actualiza el kernel en linux? How to upgrade Kernel in Linux ?

This varies by the flavor or package manager.

```
sudo yum update kernel
```
```
sudo apt update kernel
```


* How To scan newly asssigned luns on linux box without rebooting ?


* How  to find WWN numbers of HBA cards in Linux Server ?

 We can find the WWN numbers of HBA cards using the command ‘systool -c fc_host -v | grep port_name’

* How to add & change the Kernel parameters ?

 To Set the kernel parameters in linux , first edit the file ‘/etc/sysctl.conf’ after making the changes save the file and run the command ‘sysctl -p’ , this command will make the changes permanently without rebooting the machine.

####[[⬆]](#toc) <a name='cm'>Configuration Management questions:</a>

* Para que se usa puppet/chef/ansible? What is puppet/chef/ansible used for?

* Que es un Puppet Server ?

* Que son manifestos en Puppet ?


* Which Command is used to sign requested certificates in Puppet Server ?

 ‘puppetca  –sign hostname-of-agent’ in (2.X)  & ‘puppet ca  sign hostname-of-agent’ in  (3.X)

 * At which location  Puppet Master Stores Certificates ?

  /var/lib/puppet/ssl/ca/signed

 * Que es ansible?

 * Que es un playbook?

 * How do you create the ansible playbook files?

 * What file format are the playbooks on?

 * How do you create an inventory?

 * Como tu corres un playbook? How do you run a playbook?

 * Que son modulas? What are the modules?

 using the command ‘find /<directory -type f’.

 * What is load average?

 Load  Average is defined as the average sum of the number of process waiting in the run queue and number of process currently executing over the period of 1,5 and 15  minutes. Using the ‘top’ and ‘uptime’ command we find the load average of a linux sever.

####[[⬆]](#toc) <a name='medium'>Medium Linux Questions:</a>

* Que hacen estos comandos y como lo usarias?What do the following commands do and how would you use them?
 * ```tee```
 * ```awk```
 * ```tr```
 * ```cut```
 * ```tac```
 * ```curl```
 * ```wget```
 * ```watch```
 * ```head```
 * ```tail```
* What does an ```&``` after a command do?
* What does ```& disown``` after a command do?
* What is a packet filter and how does it work?
* What is Virtual Memory?
* What is swap and what is it used for?
* What is an A record, an NS record, a PTR record, a CNAME record, an MX record?
* Are there any other RRs and what are they used for?
* What is a Split-Horizon DNS?
* What is the sticky bit?
* What does the immutable bit do to a file?
* What is the difference between hardlinks and symlinks? What happens when you remove the source to a symlink/hardlink?
* What is an inode and what fields are stored in an inode?
* How to force/trigger a file system check on next reboot?
* What is SNMP and what is it used for?
* What is a runlevel and how to get the current runlevel?
* What is SSH port forwarding?
* What is the difference between local and remote port forwarding?
* What are the steps to add a user to a system without using useradd/adduser?
* What is MAJOR and MINOR numbers of special files?
* Describe the mknod command and when you'd use it.
* Describe a scenario when you get a "filesystem is full" error, but 'df' shows there is free space.
* Describe a scenario when deleting a file, but 'df' not showing the space being freed.
* Describe how 'ps' works.
* What happens to a child process that dies and has no parent process to wait for it and what’s bad about this?
* Explain briefly each one of the process states.
* How to know which process listens on a specific port?
* What is a zombie process and what could be the cause of it?
* You run a bash script and you want to see its output on your terminal and save it to a file at the same time. How could you do it?
* Explain what echo "1" > /proc/sys/net/ipv4/ip_forward does.
* Describe briefly the steps you need to take in order to create and install a valid certificate for the site https://foo.example.com.
* Can you have several HTTPS virtual hosts sharing the same IP?
* What is a wildcard certificate?
* Which Linux file types do you know?
* What is the difference between a process and a thread? And parent and child processes after a fork system call?
* What is the difference between exec and fork?
* What is "nohup" used for?
* What is the difference between these two commands?
 * ```myvar=hello```
 * ```export myvar=hello```
* How many NTP servers would you configure in your local ntp.conf?
* What does the column 'reach' mean in ```ntpq -p``` output?
* You need to upgrade kernel at 100-1000 servers, how you would do this?
* How can you get Host, Channel, ID, LUN of SCSI disk?
* How can you limit process memory usage?
* What is bash quick substitution/caret replace(^x^y)?
* Do you know of any alternative shells? If so, have you used any?
* What is a tarpipe (or, how would you go about copying everything, including hardlinks and special files, from one server to another)?
* How can you tell if the httpd package was already installed?
* How can you list the contents of a package?
* How can you determine which package is better: openssh-server-5.3p1-118.1.el6_8.x86_64 or openssh-server-6.6p1-1.el6.x86_64 ?

####[[⬆]](#toc) <a name='hard'>Hard Linux Questions:</a>

* What is a tunnel and how you can bypass a http proxy?
* What is the difference between IDS and IPS?
* What shortcuts do you use on a regular basis?
* What is the Linux Standard Base?
* What is an atomic operation?
* Your freshly configured http server is not running after a restart, what can you do?
* What kind of keys are in ~/.ssh/authorized_keys and what it is this file used for?
* I've added my public ssh key into authorized_keys but I'm still getting a password prompt, what can be wrong?
* Did you ever create RPM's, DEB's or solaris pkg's?
* What does ```:(){ :|:& };:``` do on your system?
* How do you catch a Linux signal on a script?
* Can you catch a SIGKILL?
* What's happening when the Linux kernel is starting the OOM killer and how does it choose which process to kill first?
* Describe the linux boot process with as much detail as possible, starting from when the system is powered on and ending when you get a prompt.
* What's a chroot jail?
* When trying to umount a directory it says it's busy, how to find out which PID holds the directory?
* What's LD_PRELOAD and when it's used?
* You ran a binary and nothing happened. How would you debug this?
* What are cgroups? Can you specify a scenario where you could use them?
* Name as many ways as you can to count the number of instances of a word in a file (grep -c, wc -l, etc)
* In what circumstance can df and du disagree on available disk space? How do you solve it? (locked files)
* What is the difference between hashing and encrypting?
* In as much detail as you can, explain a 3 way handshake (how deep can they go?)
* When would you want to use UDP over TCP?
* What is LVM?
* What is /proc/mdstat?




####[[⬆]](#toc) <a name='expert'>Expert Linux Questions:</a>

* A running process gets ```EAGAIN: Resource temporarily unavailable``` on reading a socket. How can you close this bad socket/file descriptor without killing the process?


####[[⬆]](#toc) <a name='network'>Networking Questions:</a>

* What is localhost and why would ```ping localhost``` fail?
* What is the similarity between "ping" & "traceroute" ? How is traceroute able to find the hops.
* What is the command used to show all open ports and/or socket connections on a machine?
* Is 300.168.0.123 a valid IPv4 address?
* Which IP ranges/subnets are "private" or "non-routable" (RFC 1918)?
* What is a VLAN?
* What is ARP and what is it used for?
* What is the difference between TCP and UDP?
* What is the purpose of a default gateway?
* What is command used to show the routing table on a Linux box?
* A TCP connection on a network can be uniquely defined by 4 things. What are those things?
* When a client running a web browser connects to a web server, what is the source port and what is the destination port of the connection?
* How do you add an IPv6 address to a specific interface?
* You have added an IPv4 and IPv6 address to interface eth0. A ping to the v4 address is working but a ping to the v6 address gives yout the response ```sendmsg: operation not permitted```. What could be wrong?
* What is SNAT and when should it be used?
* Explain how could you ssh login into a Linux system that DROPs all new incoming packets using a SSH tunnel.
* How do you stop a DDoS attack?
* How can you see content of an ip packet?
* What is IPoAC (RFC 1149)?

####[[⬆]](#toc) <a name='postgresql'>Postgresql questions:</a>
* Como crear un usuario How do you create a user?
* How do you provide privileges to a user?
* Como crear un DB?
* How do you configure the DB to be reachable from anywhere?
* Como configurar certificados y TLS?How do you configure certs and TLS?

####[[⬆]](#toc) <a name='mysql'>MySQL questions:</a>

* How do you create a user?
* How do you provide privileges to a user?
* What is the difference between a "left" and a "right" join?
* Explain briefly the differences between InnoDB and MyISAM.* How do you create a new postgres user?
* Describe briefly the steps you need to follow in order to create a simple master/slave cluster.
* Why should you run "mysql_secure_installation" after installing MySQL?
* How do you check which jobs are running?


###[[

####[[⬆]](#toc) <a name='devop'>DevOps Questions:</a>

* Can you describe your workflow when you create a script?
* Que es GIT?
* What is a dynamically/statically linked file?
* What does "./configure && make && make install" do?
* What is Nagios/Zenoss/NewRelic used for?
* What is Jenkins/TeamCity/GoCI used for?
* What is the difference between Containers and VMs?
* What is a virtual IP address? What is a cluster?
* How do you print all strings of printable characters present in a file?
* How do you find shared library dependencies?
* What is Automake and Autoconf?
* ./configure shows an error that libfoobar is missing on your system, how could you fix this, what could be wrong?
* What are the advantages/disadvantages of script vs compiled program?
* What's the relationship between continuous delivery and DevOps?
* What are the important aspects of a system of continuous integration and deployment?

####[[⬆]](#toc) <a name='fun'>Fun Questions:</a>

* A careless sysadmin executes the following command: ```chmod 444 /bin/chmod ``` - what do you do to fix this?
* I've lost my root password, what can I do?
* I've rebooted a remote server but after 10 minutes I'm still not able to ssh into it, what can be wrong?
* If you were stuck on a desert island with only 5 command-line utilities, which would you choose?
* You come across a random computer and it appears to be a command console for the universe. What is the first thing you type?
* Tell me about a creative way that you've used SSH?
* You have deleted by error a running script, what could you do to restore it?
* What will happen on 19 January 2038?
* How to reboot server when reboot command is not responding?


####[[⬆]](#toc) <a name='demo'>Demo Time:</a>

* Unpack test.tar.gz without man pages or google.
* Remove all "*.pyc" files from testdir recursively?
* Search for "my konfu is the best" in all *.py files.
* Replace the occurrence of "my konfu is the best" with "I'm a linux jedi master" in all *.txt files.
* Test if port 443 on a machine with IP address X.X.X.X is reachable.
* Get http://myinternal.webserver.local/test.html via telnet.
* How to send an email without a mail client, just on the command line?
* Write a ```get_prim``` method in python/perl/bash/pseudo.
* Find all files which have been accessed within the last 30 days.
* Explain the following command ```(date ; ps -ef | awk '{print $1}' | sort | uniq | wc -l ) >> Activity.log```
* Write a script to list all the differences between two directories.
* In a log file with contents as ```<TIME> : [MESSAGE] : [ERROR_NO] - Human readable text``` display summary/count of specific error numbers that occurred every hour or a specific hour.


####[[⬆]](#toc) <a name='references'>Other Great References:</a>

Some questions are 'borrowed' from other great references like:

* https://github.com/darcyclarke/Front-end-Developer-Interview-Questions
* https://github.com/kylejohnson/linux-sysadmin-interview-questions/blob/master/test.md
* http://slideshare.net/kavyasri790693/linux-admin-interview-questions

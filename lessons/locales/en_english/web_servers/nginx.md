#Nginx
















Two big reasons for separate partitions/file systems is for 1) different mount options to meet security requirements 2) to protect a critical system directory from becoming full when user data or log data gets out of control.

The minimum configuration I will use on EL7 is;

    /
    /home (nodev,nosuid)
    /tmp (nodev,nosuid,noexec)
    /var/tmp -> /tmp (bind mount)
    /var (nosuid)
    /var/log (nodev,nosuid,noexec)
    /var/log/audit (nodev,nosuid,noexec)
    /dev/shm (nodev,nosuid,noexec)

Then, depending on the server role you may need to further fine tune. A public facing web server would certainly benefit from /var/www mounted with more restrictive options than /var if it is hosts an application such as a wiki or a blog that writes to the path. However, noexec may break certain web sites. You'll need to test this to see if it is appropriate for your use.

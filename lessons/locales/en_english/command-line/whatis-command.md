# whatis



Whew, we’ve learned quite a bit of commands so far, if you are ever feeling doubtful about what a command does, you can use the whatis command. The whatis command provides a brief description of command line programs.
```
whatis cat
```

The description gets sourced from the manual page of each command. If you ran whatis cat, you’d see there is a small blurb with a short description.

## Configuration Management

Configuration Management can be used to automate tasks and reduce deployment time.


##Ansible

Ansible is a configuration management tool written in python and uses SSH to configure hosts. Unlike other tools, it does not require an agent to be able to work. Ansible uses the concept of playbooks and roles. A role are tasks that will be accomplished on the host and playbooks are a collection of roles and list of hosts. Playbooks are written in YAML files. For example:

* A role can be used to install postgresql, initiate the database and start the service.
* A playbook can have a group of hosts, a list of roles and other variables.

## Install Ansible
```
pip install -U ansible
```
```
sudo apt install ansible
```
```
sudo yum install epel-release
sudo yum install ansible
```


## Puppet

Puppet is an open-source & enterprise software for configuration management in linux.  Puppet is a  IT automation software used to push configuration to its clients (puppet agents) using code. Puppet uses the concept of main (Puppet Server) and client (Puppet Agent) to get instructions from the main server. The puppet agent needs to be installed in every host that you will manage. Puppet its written in ruby and uses manifest for instructions.

```
sudo yum install epel-release
sudo yum install puppet
```

## Salt
